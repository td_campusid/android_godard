package killian.m2.android.tp.com.tp_m2_android.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import killian.m2.android.tp.com.tp_m2_android.DetailsActivity;

/**
 * Created by killian on 19/12/2018.
 */

public class GraphicView extends View {
    private Paint graphicPaint;
    private Paint pointPaint;
    private Paint numbersPaint;
    private Paint rectGreenPaint;
    private Paint rectBluePaint;
    private Paint rectYellowPaint;
    private List<Paint> rectPaints;
    private String[] days = {"LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI", "DIMANCHE"};


    private float touchX;

    public GraphicView(Context context) {
        super(context);
    }

    public GraphicView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public GraphicView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        initAxesPaint();
        initNumbersPaint();
        initRectPaints();

        int width = getWidth();
        int height = getHeight();

        // DRAW AXES
        //abscisses
        canvas.drawLine(75, height-75, width/1.05f, height-75, graphicPaint);
        //ordonnées
        canvas.drawLine(75, height-75, 75, 70, graphicPaint);

        Date now = new Date();

        //coordonnées (valeurs) axes des ordonnées
        for(int j = 0; j <= 24; j++){
            canvas.drawText(String.valueOf(j), 40, height-75-j*(height-150)/24, numbersPaint);
        }

        int abscissesInterval = 120;

        //coordonnées (valeurs) axes des abscisses
        int  k = 0;
        for(int i = 0; i < 7; i++){
            float startingXPoint = abscissesInterval;

            //alterner les couleurs du graphique
            if(k == 3){
                k = 0;
            }

            //en fontion de la zone toucher on lance l'intent des details avec les infos concernant le rectangle touché
            if(touchX >= startingXPoint && touchX <= 120+abscissesInterval){
                showDetails(i);
            }

            //faire varier la valeur 'top' en fonction du temps de charge pour les 7 derniers jours récupéré via firebase
            canvas.drawRect(startingXPoint, 150, 120+abscissesInterval, height-82.5f, rectPaints.get(k));
            canvas.drawText(days[i], startingXPoint, height-50, numbersPaint);
            abscissesInterval += 120;


            k++;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            touchX = event.getX();
            float touchY = event.getY();

            String formattedParticulars = "[" + touchX + ";" + touchY + "]";

            this.invalidate();

            Toast.makeText(getContext(), formattedParticulars, Toast.LENGTH_LONG).show();
        }

        return true;
    }

    public void showDetails(int day){
        Intent detailsIntent = new Intent(getContext(), DetailsActivity.class);
        detailsIntent.putExtra("day", day);

        getContext().startActivity(detailsIntent);
    }

    private void initAxesPaint(){
        graphicPaint = new Paint();
        graphicPaint.setStyle(Paint.Style.STROKE);
        graphicPaint.setStrokeWidth(10);
        graphicPaint.setColor(Color.BLACK);
    }

    private void initNumbersPaint(){
        numbersPaint = new Paint();
        numbersPaint.setTextSize(20);
        numbersPaint.setColor(Color.BLACK);
    }

    private void initRectPaints(){
        rectBluePaint = new Paint();
        rectGreenPaint = new Paint();
        rectYellowPaint = new Paint();

        rectBluePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        rectGreenPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        rectYellowPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        rectBluePaint.setStrokeWidth(7.5f);
        rectGreenPaint.setStrokeWidth(7.5f);
        rectYellowPaint.setStrokeWidth(7.5f);

        rectBluePaint.setColor(Color.BLUE);
        rectGreenPaint.setColor(Color.GREEN);
        rectYellowPaint.setColor(Color.YELLOW);

        rectPaints = new ArrayList<>();
        rectPaints.add(rectBluePaint);
        rectPaints.add(rectGreenPaint);
        rectPaints.add(rectYellowPaint);
    }

}
