package killian.m2.android.tp.com.tp_m2_android.Service;

import java.util.List;

import killian.m2.android.tp.com.tp_m2_android.Model.PowerEvent;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by killian on 07/12/2018.
 */

public interface PowerService {
    @GET("/powerevents/{id}.json")
    Call<PowerEvent> getPowerEvent(@Path("id") String id);

    @PUT("/powerevents/{id}.json")
    Call<PowerEvent> createPowerEvent(@Path("id") String id, @Body PowerEvent powerEvent);
}
