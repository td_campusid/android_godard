package killian.m2.android.tp.com.tp_m2_android.Enums;

/**
 * Created by killian on 07/12/2018.
 */

public enum ActionPower {
    CONNECTED, DISCONNECTED
}
