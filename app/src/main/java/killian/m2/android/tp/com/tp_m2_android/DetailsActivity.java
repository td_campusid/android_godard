package killian.m2.android.tp.com.tp_m2_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //On recupère le jour sous forme de int et en fonction de celui-ci on fait va chercher la donnée correspondante dans firebase
        this.getIntent().getExtras().get("day");

    }
}
