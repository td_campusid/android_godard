package killian.m2.android.tp.com.tp_m2_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Calendar;

import killian.m2.android.tp.com.tp_m2_android.Enums.ActionPower;
import killian.m2.android.tp.com.tp_m2_android.Model.PowerEvent;
import killian.m2.android.tp.com.tp_m2_android.Service.PowerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private Retrofit retrofit;
    private PowerService powerService;
    private String apiUrl = "https://tp-android-m2-killian.firebaseio.com";
    private TextView powerEventView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = new Retrofit.Builder().baseUrl(apiUrl).addConverterFactory(GsonConverterFactory.create()).build();
        powerService = retrofit.create(PowerService.class);

        powerEventView = findViewById(R.id.powerEventView);

        showPowerEvent();
    }

    public void showPowerEvent(){
        String id = ActionPower.CONNECTED + "_" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        powerService.getPowerEvent(id).enqueue(new Callback<PowerEvent>(){
            @Override
            public void onResponse(Call<PowerEvent> call, Response<PowerEvent> response) {
                System.out.println("response : " + response.message() + " / code : " + response.code());

                if(response.body() != null){
                    PowerEvent powerEvent = response.body();

                    powerEventView.setText(powerEvent.getId() + " : charging duration (in seconds) : " + powerEvent.getSecondDuration());
                }
            }

            @Override
            public void onFailure(Call<PowerEvent> call, Throwable t) {
                System.out.println("ERROR");
            }
        });
    }
}
