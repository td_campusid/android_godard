package killian.m2.android.tp.com.tp_m2_android.Model;

import java.util.Date;

import killian.m2.android.tp.com.tp_m2_android.Enums.ActionPower;

/**
 * Created by killian on 07/12/2018.
 */

public class PowerEvent {
    private String id;

    private ActionPower action;
    private Date hour;
    private long secondDuration;

    public PowerEvent(String id, ActionPower action, Date hour){
        this.id = id;
        this.action = action;
        this.hour = hour;
    }

    public long getSecondDuration() {
        return secondDuration;
    }

    public void setSecondDuration(long secondDuration) {
        this.secondDuration = secondDuration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ActionPower getAction() {
        return action;
    }

    public void setAction(ActionPower action) {
        this.action = action;
    }

    public Date getHour() {
        return hour;
    }

    public void setHour(Date hour) {
        this.hour = hour;
    }
}
