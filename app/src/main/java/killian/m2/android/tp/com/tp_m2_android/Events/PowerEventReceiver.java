package killian.m2.android.tp.com.tp_m2_android.Events;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.Locale;

import killian.m2.android.tp.com.tp_m2_android.Enums.ActionPower;
import killian.m2.android.tp.com.tp_m2_android.MainActivity;
import killian.m2.android.tp.com.tp_m2_android.Model.PowerEvent;
import killian.m2.android.tp.com.tp_m2_android.Service.PowerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by killian on 16/12/2018.
 */

public class PowerEventReceiver extends BroadcastReceiver {

    private PowerService powerService;
    private Retrofit retrofit;
    private String apiUrl = "https://tp-android-m2-killian.firebaseio.com";
    private long powerTime;
    private PowerEvent powerEvent;
    private PowerEvent powerConnectEvent;

    @Override
    public void onReceive(final Context context, Intent intent) {
        String actionType = intent.getAction();

        retrofit = new Retrofit.Builder().baseUrl(apiUrl).addConverterFactory(GsonConverterFactory.create()).build();
        powerService = retrofit.create(PowerService.class);

        powerEvent = null;
        Calendar calendar = Calendar.getInstance(new Locale("fr", "FR"));

        String id = "";

        if(actionType.equals("android.intent.action.ACTION_POWER_CONNECTED")){
            id = ActionPower.CONNECTED + "_" + calendar.get(Calendar.DAY_OF_MONTH);

            if(powerConnectEvent != null && powerConnectEvent.getSecondDuration() != 0){
                powerTime = System.nanoTime();
            } else {
                powerTime = 0;
            }

            powerEvent = new PowerEvent(id, ActionPower.CONNECTED, calendar.getTime());
        } else if(actionType.equals("android.intent.action.ACTION_POWER_DISCONNECTED")){
            id = ActionPower.DISCONNECTED + "_" + calendar.get(Calendar.DAY_OF_MONTH);

            powerTime = System.nanoTime();

            powerService.getPowerEvent(ActionPower.CONNECTED + "_" + calendar.get(Calendar.DAY_OF_MONTH)).enqueue(new Callback<PowerEvent>()
            {
                @Override
                public void onResponse(Call<PowerEvent> call, Response<PowerEvent> response) {
                    powerConnectEvent = response.body();

                    long ellapsedTime = powerTime / (long) 1_000_000_000.0;
                    long actualSecondDuration = powerConnectEvent.getSecondDuration() + ellapsedTime;

                    powerConnectEvent.setSecondDuration(actualSecondDuration);

                    Call<PowerEvent> firstCall = powerService.createPowerEvent(powerConnectEvent.getId(), powerConnectEvent);
                    firstCall.enqueue(new Callback<PowerEvent>() {
                        @Override
                        public void onResponse(Call<PowerEvent> call, Response<PowerEvent> response) {
                            System.out.println("message : " + response.message() + " / code : " + response.code());
                        }

                        @Override
                        public void onFailure(Call<PowerEvent> call, Throwable t) {
                            System.out.println("Failed / " + t.getMessage());
                        }
                    });

                    System.out.println("Setting duration success");
                }

                @Override
                public void onFailure(Call<PowerEvent> call, Throwable t) {
                    System.out.println("Setting duration failed");
                }
            });

            powerEvent = new PowerEvent(id, ActionPower.DISCONNECTED, calendar.getTime());
        }

        Call<PowerEvent> secondCall = powerService.createPowerEvent(id, powerEvent);
        secondCall.enqueue(new Callback<PowerEvent>() {
            @Override
            public void onResponse(Call<PowerEvent> call, Response<PowerEvent> response) {
                System.out.println("message : " + response.message() + " / code : " + response.code());
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            }

            @Override
            public void onFailure(Call<PowerEvent> call, Throwable t) {
                System.out.println("Failed / " + t.getMessage());
            }
        });
    }
}
